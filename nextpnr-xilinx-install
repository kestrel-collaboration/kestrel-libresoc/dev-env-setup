#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root using 'sudo bash'"
  exit
fi

# change into $SUDO_USER home directory
cd /home/$SUDO_USER
mkdir -p src/nextpnr-xilinx
cd src/nextpnr-xilinx

apt-get install -y libcurl3-gnutls git git-man

git clone https://github.com/f4pga/prjxray.git
git clone https://github.com/SymbiFlow/prjxray-db.git
git clone https://github.com/gatecat/nextpnr-xilinx.git

## Steps to compile prjxray

### Necessary software to install

apt-get install -y build-essential make cmake python3 python3-setuptools \
                   python3-dev python3-numpy cython3 python3-pip

pip3 install textx

# this really doesn't work properly, antlr is still not correctly
# detected, (missing ANTLRconfig.cmake) but at least fasm falls back
# to the python version
sudo apt install cmake default-jre-headless uuid-dev libantlr4-runtime-dev

apt-get install -y wget
wget https://files.pythonhosted.org/packages/78/4c/94fb3bdb87bea21406c0e5da375f0b10d7b1e4b5103cea453a2de23b5d61/fasm-0.0.2.post88.tar.gz
tar -xvzf fasm-0.0.2.post88.tar.gz
cd fasm-0.0.2.post88
python3 setup.py install
cd ..

### Build prjxray

cd prjxray
git checkout 18b92012afe2b03f3f975a78c4372c74b60dca0c
git submodule update --init --recursive
mkdir build; cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/nextpnr-xilinx ..
make -j$(nproc)
make install
install -d -m 0755 /usr/local/nextpnr-xilinx/build/tools
install -m 0755 tools/{bitread,bittool,frame_address_decoder,gen_part_base_yaml,segmatch,xc7frames2bit,xc7patch} \
/usr/local/nextpnr-xilinx/build/tools
cd ..
cp -dpr utils /usr/local/nextpnr-xilinx
sed -i -e '/^# Vivado /,$d' /usr/local/nextpnr-xilinx/utils/environment.sh
python3 setup.py develop
cd ..

## Steps to compile prjxray-db

### Install prjxray-db

cd prjxray-db
git archive --format=tar --prefix=database/ \
 0a0addedd73e7e4139d52a6d8db4258763e0f1f3 | \
 tar -C /usr/local/nextpnr-xilinx -xf -
cd ..

## Steps to compile nextpnr-xilinx

### Necessary software to install

apt-get install -y libboost-thread-dev libboost-iostreams-dev \
 libboost-program-options-dev libeigen3-dev libboost-python-dev \
 libboost-filesystem-dev

### Build nextpnr-xilinx

cd nextpnr-xilinx
git checkout 565588a69ea95a52f7c7592f4ed81d9bef6cfb60
git submodule init
git submodule update
cmake -DARCH=xilinx -DBUILD_GUI=OFF \
	-DCMAKE_INSTALL_PREFIX=/usr/local/nextpnr-xilinx .
make -j$(nproc)
make install
ln -s xc7a100tcsg324-1 xilinx/external/prjxray-db/artix7/xc7a100t
python3 xilinx/python/bbaexport.py --device xc7a100tcsg324-1 \
 --bba xilinx/xc7a100t.bba
./bbasm --l xilinx/xc7a100t.bba xilinx/xc7a100t.bin
install -d -m 0755 /usr/local/nextpnr-xilinx/share/xilinx
install -m 0755 xilinx/xc7a100t.bin /usr/local/nextpnr-xilinx/share/xilinx
export XRAY_DIR=/usr/local/nextpnr-xilinx
cd ..

cd /home/$SUDO_USER/src/nextpnr-xilinx
chown -R $SUDO_USER .
chgrp -R $SUDO_USER .

